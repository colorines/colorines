import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface ColorName {
  hexcode: string;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class ColorNameService {

readonly colorNameServiceURL = 'https://cors-anywhere.herokuapp.com/https://colornames.org/search/json/';
constructor(private http: HttpClient) {}

getColorName(colorHex: string): Observable<ColorName>{
  return this.http.get(this.colorNameServiceURL, { params: {hex: colorHex}}) as Observable<ColorName>;
}

}
