import { RGBPixel } from './../colors/models/colors';
import { Component, OnInit } from '@angular/core';
import { Pixel } from '../models/pixel';
import { getHexCode, rgbToHsv } from '../colors/functions.ts/transform';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css']
})



export class FileUploaderComponent implements OnInit {
  public imagePath;
  imgUrl: any;
  imgWidth: number;
  height: number;
  colors: string[];

  constructor() {
    this.height = 200;
   }

  ngOnInit(): void {}

  handleChange(files) {
    if (files.length === 0) {
       return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_) => {
      this.imgUrl = reader.result;
    };
  }

  onImageLoad($event) {
    if ($event?.target) {
      console.log($event.target);
      this.imgWidth = $event.target.width;
      this.height = $event.target.height * 0.3;

      const rawPixelsData = this.getImageData('my-image');
      const pixels = new Array<RGBPixel>();

      for (let i = 0; i < rawPixelsData.length; i += 4) {
        pixels.push(new RGBPixel(rawPixelsData[i], rawPixelsData[i + 1], rawPixelsData[i + 2]));
      }

      const palettePixels = new Array<RGBPixel>();

      for (let i = 0; i < 10; i += 1) {
        palettePixels.push(pixels[Math.floor(Math.random() * pixels.length)]);
      }

      console.log(palettePixels);

      this.colors = palettePixels
                      .sort( (pixelA, pixelB) =>
                        (rgbToHsv(pixelA).hue - rgbToHsv(pixelB).hue) )
                      .map((pixel) => getHexCode(pixel));

      console.log(this.colors);

    }
  }

  getImageData(imageId: string) {
    const img: any = document.getElementById(imageId);
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    const context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, canvas.width, canvas.height);
    return context.getImageData(0, 0, canvas.width, canvas.height).data;
  }

}
