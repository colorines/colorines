export class Pixel {
  constructor(public red: number, public green: number, public blue: number){}
  getHexCode(): string {
    return '#' + this.toHex(this.red) + this.toHex(this.green) + this.toHex(this.blue);
  }

  private toHex(num: number): string {
    const hexValue = num.toString(16);
    return hexValue.length < 2
      ? '0' + hexValue
      : hexValue;
  }
}
