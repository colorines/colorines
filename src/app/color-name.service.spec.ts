/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ColorNameService } from './color-name.service';

describe('Service: ColorName', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ColorNameService]
    });
  });

  it('should ...', inject([ColorNameService], (service: ColorNameService) => {
    expect(service).toBeTruthy();
  }));
});
