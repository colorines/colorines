import { ColorNameService } from './../color-name.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';


@Component({
  selector: 'app-color-palette',
  templateUrl: './color-palette.component.html',
  styleUrls: ['./color-palette.component.css']
})
export class ColorPaletteComponent implements OnInit, OnChanges{

  @Input()
  width: number;

  @Input()
  height: number;

  @Input()
  colors: string[];

  colorNames: Map<string, string>;



  constructor(private colorNameService: ColorNameService,
              private cdr: ChangeDetectorRef) {
    this.colorNames = new Map<string, string>();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('colors' in changes) {
      console.log('calling color service');
      this.colors.forEach(color => {
        this.colorNameService.getColorName(color.replace('#', '')).subscribe(colorName => {
          console.log(color + colorName.name);
          this.colorNames.set(color, colorName.name);
        });
      });
    }
  }

  getColorTooltip(color: string) {
    if (this.colorNames.get(color)) {
      return color + ' ' + this.colorNames.get(color);
    }
    return color;
  }
}
