import { RGBPixel, HSVPixel, NormalizedRGBPixel } from './../models/colors';


export function rgbToHsv(rgbPixel: RGBPixel): HSVPixel {
  const normRgbPixel = normalizeRGBPixel(rgbPixel);
  const min = Math.min(normRgbPixel.red, normRgbPixel.green, normRgbPixel.blue);
  const max = Math.max(normRgbPixel.red, normRgbPixel.green, normRgbPixel.blue);
  if (min === max) {
    return new HSVPixel(0, 0, max);
  }

  const delta = max - min;
  const value = max;
  const saturation = delta / max;

  let hue;
  if ( normRgbPixel.red === max) {
    hue = 60 * (normRgbPixel.green - normRgbPixel.blue) / delta;
  } else if (normRgbPixel.green === max) {
    hue = 60 * (2 + (normRgbPixel.blue - normRgbPixel.red) / delta);
  } else {
    hue = 60 * (4 + (normRgbPixel.red - normRgbPixel.green) / delta);
  }

  if (hue < 0) {
    hue += 360;
  }

  return new HSVPixel(hue, saturation, value);
}

export function normalizeRGBPixel(rgbPixel: RGBPixel): NormalizedRGBPixel {
  return new NormalizedRGBPixel(rgbPixel.red / 255, rgbPixel.green / 255, rgbPixel.blue / 255);
}

export function denormalizeRGBPixel(normRgbPixel: NormalizedRGBPixel) {
  return new RGBPixel(Math.round(normRgbPixel.red * 255), Math.round(normRgbPixel.green * 255), Math.round(normRgbPixel.blue * 255));
}


export function getHexCode(rgbPixel: RGBPixel) {
  return '#' + toHex(rgbPixel.red) + toHex(rgbPixel.green) + toHex(rgbPixel.blue);
}

export function toHex(num: number): string {
  const hexValue = num.toString(16);
  return hexValue.length < 2
    ? '0' + hexValue
    : hexValue;
}
