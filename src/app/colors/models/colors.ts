export class RGBPixel {
  constructor(public red: number, public green: number, public blue: number) {}
}

export class NormalizedRGBPixel {
  constructor(public red: number, public green: number, public blue: number) {}
}

export class HSVPixel {
  constructor(public hue: number, public saturation: number, public value: number) {}
}
